###### Boilerplate ######

.POSIX:  # Ensure POSIX Makefile syntax
.SUFFIXES:  # Empty suffix list
.SCCS_GET:  # Prevent SCCS magic

###### Project info ######

TARGET ?= main
OBJS ?= main.o
SRCS ?= main.cpp
HEADERS ?= debug.h
ARCHIVE ?= release

####### Build options ######

# {debug,harden,fastdebug,release}
BUILD_TYPE ?= debug
DEBUG_GLIBCXX ?= true
DEBUG_ASAN ?= true
DEBUG_MSAN ?= false
DEBUG_TSAN ?= false
DEBUG_OPT ?= true
DEBUG_COV ?= false
# {gcc,clang,clanglifetime,icc,emscripten,other}
CCVARIANT ?= clang
USE_CONAN ?= false

###### Tool names ######

CC.gcc = $(GCC)
CC.clang = $(CLANG)
CC.clanglifetime = $(CLANG_LIFETIME)
CC.icc = $(ICC)
CC.emscripten = $(EMCC)
CC.other = cc
CXX.gcc = $(GXX)
CXX.clang = $(CLANGXX)
CXX.clanglifetime = $(CLANGXX_LIFETIME)
CXX.icc = $(ICC)
CXX.emscripten = $(EMCXX)
CXX.other = c++

CC = $(CC.$(CCVARIANT))
CXX = $(CXX.$(CCVARIANT))

# Other compilers
#CC = zig cc -target x86_64-linux-musl
#CXX = zig c++ -target x86_64-linux-musl
#CC = zig cc -target mips-linux-gnu
#CXX = zig c++ -target mips-linux-gnu
#CC = zig cc -target wasm32-wasi-musl
#CXX = zig c++ -target wasm32-wasi-musl
#CC = sudo podman run -v "$$PWD:/src" -w /src -it --rm circle circle
#CXX = sudo podman run -v "$$PWD:/src" -w /src -it --rm circle circle

CLANG ?= clang
CLANGXX ?= clang++
CLANG_LIFETIME ?= ~/apps/llvm-project/build/bin/clang
CLANGXX_LIFETIME ?= ~/apps/llvm-project/build/bin/clang++
GCC ?= gcc
GXX ?= g++
EMCC ?= emcc
EMCXX ?= em++
ICC ?= sudo podman run -v "$$PWD:/src" -w /src -it --rm \
	--entrypoint "/opt/intel/oneapi/compiler/2021.4.0/linux/bin/intel64/icc" \
	intel/oneapi-hpckit

CP ?= cp
FIND ?= find
GREP ?= grep
MKDIR ?= mkdir
RM = rm -rf
TAR ?= tar
CAT ?= cat

BEAR ?= bear
CONAN ?= conan
CLANG_FORMAT ?= clang-format
CLANG_TIDY ?= clang-tidy
CPPCHECK ?= cppcheck
TSCANCODE ?= tscancode
CPPLINT ?= cpplint
FLAWFINDER ?= flawfinder
OCLINT ?= oclint
INFER ?= infer
VALGRIND ?= valgrind
PVS_STUDIO ?= pvs-studio
PVS_STUDIO_HOWTO ?= how-to-use-pvs-studio-free
PLOG_CONVERTER ?= plog-converter
IKOS ?= sudo podman run --rm -v $$PWD:/src ikos ikos
IKOS_REPORT ?= \
	sudo podman run --rm -v $$PWD:/src ikos ikos-report
FLINT ?= flint++
LLVM_PROFDATA ?= llvm-profdata
LLVM_COV ?= llvm-cov
IWYU ?= iwyu-tool
HARDENED_MALLOC ?= /usr/lib/libhardened_malloc.so
COV_BUILD ?= /opt/cov-analysis/bin/cov-build
PCLINT ?= pclint
PCLINT_CONFIG ?= pclp_config.py

###### Compiler flags ######

CFLAGS.debug.asan.true += \
	-fsanitize=address \
	-fsanitize-address-use-after-scope \
	-fsanitize=pointer-compare \
	-fsanitize=pointer-subtract
CFLAGS.debug.msan.true += \
	-fsanitize=memory \
	-fsanitize-memory-track-origins \
	-stdlib=libc++
CFLAGS.debug.tsan.true += -fsanitize=thread
CFLAGS.debug.glibcxx.true += -D_GLIBCXX_DEBUG -D_GLIBCXX_DEBUG_PEDANTIC
# _FORTIFY_SOURCE only available when optimizations are enabled
CFLAGS.debug.opt.true += -Og -D_FORTIFY_SOURCE=2
CFLAGS.debug.opt.false += -O0
CFLAGS.debug.cov.true += -fprofile-instr-generate -fcoverage-mapping

CFLAGS.debug.clang += -Weverything
CFLAGS.debug.clang += -Wno-c++98-compat-pedantic
CFLAGS.debug.clang += -Wno-missing-prototypes
CFLAGS.debug.clang += -Wno-missing-variable-declarations
CFLAGS.debug.clang += -DDEBUG
CFLAGS.debug.clang += $(CFLAGS.debug.glibcxx.$(DEBUG_GLIBCXX))
CFLAGS.debug.clang += -D_GLIBCXX_SANITIZE_VECTOR
CFLAGS.debug.clang += -D_GLIBCPP_CONCEPT_CHECKS
CFLAGS.debug.clang += $(CFLAGS.debug.asan.$(DEBUG_ASAN))
CFLAGS.debug.clang += $(CFLAGS.debug.msan.$(DEBUG_MSAN))
CFLAGS.debug.clang += $(CFLAGS.debug.tsan.$(DEBUG_TSAN))
CFLAGS.debug.clang += -fsanitize=cfi
CFLAGS.debug.clang += -fno-sanitize=cfi-icall
CFLAGS.debug.clang += -fsanitize=undefined
CFLAGS.debug.clang += -fsanitize=implicit-conversion,integer
CFLAGS.debug.clang += -fsanitize=nullability,local-bounds
CFLAGS.debug.clang += -fsanitize=float-divide-by-zero,float-cast-overflow
CFLAGS.debug.clang += -fcf-protection=full
CFLAGS.debug.clang += -fstack-protector-all
CFLAGS.debug.clang += -fstack-clash-protection
CFLAGS.debug.clang += -fPIE
CFLAGS.debug.clang += $(CFLAGS.debug.opt.$(DEBUG_OPT))
CFLAGS.debug.clang += -march=native
CFLAGS.debug.clang += -fopenmp
CFLAGS.debug.clang += -flto
CFLAGS.debug.clang += -fvisibility=hidden
CFLAGS.debug.clang += -fwhole-program-vtables
CFLAGS.debug.clang += -mseses
CFLAGS.debug.clang += -ggdb3
CFLAGS.debug.clang += -fstandalone-debug
CFLAGS.debug.clang += $(CFLAGS.debug.cov.$(DEBUG_COV))

CFLAGS.debug.gcc += -DDEBUG
CFLAGS.debug.gcc += $(CFLAGS.debug.glibcxx.$(DEBUG_GLIBCXX))
CFLAGS.debug.gcc += -D_GLIBCXX_CONCEPT_CHECKS
CFLAGS.debug.gcc += -D_GLIBCXX_SANITIZE_VECTOR
CFLAGS.debug.gcc += $(CFLAGS.debug.asan.$(DEBUG_ASAN))
CFLAGS.debug.gcc += $(CFLAGS.debug.tsan.$(DEBUG_TSAN))
CFLAGS.debug.gcc += -fsanitize=undefined
CFLAGS.debug.gcc += -fsanitize=float-divide-by-zero,float-cast-overflow
CFLAGS.debug.gcc += -fstack-protector-all
CFLAGS.debug.gcc += -fstack-clash-protection
CFLAGS.debug.gcc += -fPIE
CFLAGS.debug.gcc += $(CFLAGS.debug.opt.$(DEBUG_OPT))
CFLAGS.debug.gcc += -march=native
CFLAGS.debug.gcc += -fopenmp
CFLAGS.debug.gcc += -flto
CFLAGS.debug.gcc += -fvisibility=hidden
CFLAGS.debug.gcc += -mindirect-branch=thunk
CFLAGS.debug.gcc += -mfunction-return=thunk
CFLAGS.debug.gcc += -mindirect-branch-register
CFLAGS.debug.gcc += -ggdb3
# (Almost) all warnings, generated with `g++ -Wall -Wextra -Q --help=warnings`
CFLAGS.debug.gcc += -Wabi-tag
CFLAGS.debug.gcc += -Waligned-new=all
CFLAGS.debug.gcc += -Wall
CFLAGS.debug.gcc += -Walloc-zero
CFLAGS.debug.gcc += -Walloca
CFLAGS.debug.gcc += -Warith-conversion
CFLAGS.debug.gcc += -Warray-parameter=2
CFLAGS.debug.gcc += -Wbad-function-cast
CFLAGS.debug.gcc += -Wc++-compat
CFLAGS.debug.gcc += -Wc++17-compat
CFLAGS.debug.gcc += -Wcast-qual
CFLAGS.debug.gcc += -Wcatch-value=3
CFLAGS.debug.gcc += -Wclass-conversion
CFLAGS.debug.gcc += -Wclass-memaccess
CFLAGS.debug.gcc += -Wcomma-subscript
CFLAGS.debug.gcc += -Wconditionally-supported
CFLAGS.debug.gcc += -Wconversion
CFLAGS.debug.gcc += -Wconversion-null
CFLAGS.debug.gcc += -Wctad-maybe-unsupported
CFLAGS.debug.gcc += -Wctor-dtor-privacy
CFLAGS.debug.gcc += -Wdate-time
CFLAGS.debug.gcc += -Wdelete-incomplete
CFLAGS.debug.gcc += -Wdelete-non-virtual-dtor
CFLAGS.debug.gcc += -Wdeprecated-copy
CFLAGS.debug.gcc += -Wdeprecated-copy-dtor
CFLAGS.debug.gcc += -Wdeprecated-enum-enum-conversion
CFLAGS.debug.gcc += -Wdeprecated-enum-float-conversion
CFLAGS.debug.gcc += -Wdouble-promotion
CFLAGS.debug.gcc += -Wduplicated-branches
CFLAGS.debug.gcc += -Wduplicated-cond
CFLAGS.debug.gcc += -Weffc++
CFLAGS.debug.gcc += -Wextra
CFLAGS.debug.gcc += -Wextra-semi
CFLAGS.debug.gcc += -Wfloat-conversion
CFLAGS.debug.gcc += -Wfloat-equal
CFLAGS.debug.gcc += -Wformat-nonliteral
CFLAGS.debug.gcc += -Wformat-overflow=2
CFLAGS.debug.gcc += -Wformat-security
CFLAGS.debug.gcc += -Wformat-signedness
CFLAGS.debug.gcc += -Wformat-truncation=2
CFLAGS.debug.gcc += -Wformat-y2k
CFLAGS.debug.gcc += -Wformat=2
CFLAGS.debug.gcc += -Winaccessible-base
CFLAGS.debug.gcc += -Winherited-variadic-ctor
CFLAGS.debug.gcc += -Winit-list-lifetime
CFLAGS.debug.gcc += -Winit-self
CFLAGS.debug.gcc += -Winvalid-offsetof
CFLAGS.debug.gcc += -Winvalid-pch
CFLAGS.debug.gcc += -Wjump-misses-init
CFLAGS.debug.gcc += -Wliteral-suffix
CFLAGS.debug.gcc += -Wlogical-op
CFLAGS.debug.gcc += -Wmismatched-new-delete
CFLAGS.debug.gcc += -Wmismatched-tags
CFLAGS.debug.gcc += -Wmissing-declarations
CFLAGS.debug.gcc += -Wmissing-format-attribute
CFLAGS.debug.gcc += -Wmissing-include-dirs
CFLAGS.debug.gcc += -Wmissing-prototypes
CFLAGS.debug.gcc += -Wmultichar
CFLAGS.debug.gcc += -Wmultiple-inheritance
CFLAGS.debug.gcc += -Wnested-externs
CFLAGS.debug.gcc += -Wnoexcept
CFLAGS.debug.gcc += -Wnoexcept-type
CFLAGS.debug.gcc += -Wnon-template-friend
CFLAGS.debug.gcc += -Wnon-virtual-dtor
CFLAGS.debug.gcc += -Wnormalized
CFLAGS.debug.gcc += -Wold-style-cast
CFLAGS.debug.gcc += -Woverlength-strings
CFLAGS.debug.gcc += -Woverloaded-virtual
CFLAGS.debug.gcc += -Wpedantic
CFLAGS.debug.gcc += -Wpessimizing-move
CFLAGS.debug.gcc += -Wplacement-new=2
CFLAGS.debug.gcc += -Wpmf-conversions
CFLAGS.debug.gcc += -Wpointer-arith
CFLAGS.debug.gcc += -Wrange-loop-construct
CFLAGS.debug.gcc += -Wredundant-decls
CFLAGS.debug.gcc += -Wredundant-move
CFLAGS.debug.gcc += -Wredundant-tags
CFLAGS.debug.gcc += -Wregister
CFLAGS.debug.gcc += -Wreorder
CFLAGS.debug.gcc += -Wshift-overflow=2
CFLAGS.debug.gcc += -Wsign-conversion
CFLAGS.debug.gcc += -Wsign-promo
CFLAGS.debug.gcc += -Wsized-deallocation
CFLAGS.debug.gcc += -Wstrict-aliasing=3
CFLAGS.debug.gcc += -Wstrict-null-sentinel
# CFLAGS.debug.gcc += -Wstrict-overflow=5
CFLAGS.debug.gcc += -Wstrict-prototypes
CFLAGS.debug.gcc += -Wstringop-overflow=4
CFLAGS.debug.gcc += -Wsubobject-linkage
CFLAGS.debug.gcc += -Wsuggest-attribute=format
CFLAGS.debug.gcc += -Wsuggest-override
CFLAGS.debug.gcc += -Wswitch-default
CFLAGS.debug.gcc += -Wswitch-enum
CFLAGS.debug.gcc += -Wsynth
CFLAGS.debug.gcc += -Wterminate
CFLAGS.debug.gcc += -Wundef
CFLAGS.debug.gcc += -Wunsuffixed-float-constants
CFLAGS.debug.gcc += -Wunused-const-variable=2
CFLAGS.debug.gcc += -Wunused-macros
CFLAGS.debug.gcc += -Wuseless-cast
CFLAGS.debug.gcc += -Wvariadic-macros
CFLAGS.debug.gcc += -Wvexing-parse
CFLAGS.debug.gcc += -Wvirtual-inheritance
CFLAGS.debug.gcc += -Wvirtual-move-assign
CFLAGS.debug.gcc += -Wvolatile
CFLAGS.debug.gcc += -Wwrite-strings
CFLAGS.debug.gcc += -Wzero-as-null-pointer-constant

CFLAGS.debug.other += -DDEBUG -W -g

CFLAGS.fastdebug.gcc += \
	-DDEBUG \
	-D_GLIBCXX_ASSERTIONS \
	-Ofast \
	-march=native \
	-fopenmp \
	-flto \
	-fvisibility=hidden \
	-ggdb3 \
	-Wall \
	-Wextra
CFLAGS.fastdebug.clang += \
	-DDEBUG \
	-D_GLIBCXX_ASSERTIONS \
	-Ofast \
	-march=native \
	-fopenmp \
	-flto \
	-fwhole-program-vtables \
	-fvisibility=hidden \
	-ggdb3 \
	-Wall \
	-Wextra
CFLAGS.fastdebug.other += -DDEBUG -D_GLIBCXX_ASSERTIONS -W -g -O1

CFLAGS.release.gcc += \
	-DNDEBUG \
	-Ofast \
	-march=native \
	-funroll-loops \
	-flto \
	-fopenmp \
	-fvisibility=hidden \
	-Wall \
	-Wextra \
	-g3
CFLAGS.release.clang += \
	-DNDEBUG \
	-Ofast \
	-march=native \
	-funroll-loops \
	-flto \
	-fopenmp \
	-fwhole-program-vtables \
	-fvisibility=hidden \
	-Wall \
	-Wextra \
	-g3
CFLAGS.release.other += -DNDEBUG -W -O1 -g

CFLAGS.harden.gcc += \
	-D_FORTIFY_SOURCE=2 \
	-D_GLIBCXX_ASSERTIONS \
	-Wformat -Werror=format-security \
	-fstack-clash-protection \
	-fstack-protector-all \
	-ftrapv \
	-mindirect-branch=thunk \
	-mfunction-return=thunk \
	-mindirect-branch-register \
	-Ofast \
	-march=native \
	-flto \
	-fopenmp \
	-fvisibility=hidden \
	-fPIE \
	-Wall \
	-Wextra
CFLAGS.harden.clang += \
	-D_FORTIFY_SOURCE=2 \
	-D_GLIBCXX_ASSERTIONS \
	-Wformat -Werror=format-security \
	-fsanitize=cfi \
	-fno-sanitize=cfi-icall \
	-fsanitize=undefined \
	-fsanitize-minimal-runtime \
	-fstack-clash-protection \
	-fstack-protector-all \
	-fcf-protection \
	-mretpoline \
	-Ofast \
	-march=native \
	-flto \
	-fopenmp \
	-fwhole-program-vtables \
	-fvisibility=hidden \
	-fPIE \
	-Wall \
	-Wextra
CFLAGS.harden.other += -D_FORTIFY_SOURCE=2 -D_GLIBCXX_ASSERTIONS -W -O1

CFLAGS.debug.icc += $(CFLAGS.debug.gcc)
CFLAGS.harden.icc += $(CFLAGS.harden.gcc)
CFLAGS.fastdebug.icc += $(CFLAGS.fastdebug.gcc)
CFLAGS.release.icc += $(CFLAGS.release.gcc)

CFLAGS.debug.clanglifetime += $(CFLAGS.debug.other) -Wall -Wlifetime
CFLAGS.harden.clanglifetime += $(CFLAGS.harden.other) -Wall -Wlifetime
CFLAGS.fastdebug.clanglifetime += $(CFLAGS.fastdebug.other) -Wall -Wlifetime
CFLAGS.release.clanglifetime += $(CFLAGS.release.other) -Wall -Wlifetime

CFLAGS.debug.emscripten += $(CFLAGS.debug.other)
CFLAGS.harden.emscripten += $(CFLAGS.harden.other)
CFLAGS.fastdebug.emscripten += $(CFLAGS.fastdebug.other)
CFLAGS.release.emscripten += $(CFLAGS.release.other)

##### Possibly useful optimization flags #####
# -funroll-loops
# -fvariable-expansion-in-unroller
# -fno-stack-protector
# -fdevirtualize-at-ltrans
# -fipa-pta
# -fira-loop-pressure
# -fwhole-program
# -fno-exceptions
# -fdelete-dead-exceptions
# -ffinite-loops
# -flive-range-shrinkage
# -floop-nest-optimize
# -fgraphite
# -fgraphite-identity
# -frename-registers
# -ftree-lrs
#
# -funswitch-loops
# -fsplit-loops
# -ftracer
# -fgcse-sm
# -fgcse-las
# -fsched-pressure
#
# -fmodulo-sched
# -fmodulo-sched-allow-regmoves
# -freschedule-modulo-scheduled-loops
#
# -fnothrow-opt
# -freg-struct-return
# -fshort-enums

CFLAGS += $(CFLAGS.$(BUILD_TYPE).$(CCVARIANT)) -std=gnu18 $(CPPFLAGS)
CXXFLAGS += $(CFLAGS.$(BUILD_TYPE).$(CCVARIANT)) -std=c++17 $(CPPFLAGS)

LDFLAGS.debug.gcc += $(LDFLAGS.harden.gcc)
LDFLAGS.debug.clang += $(LDFLAGS.harden.clang)
# May be needed for __int128
#LDFLAGS.debug.clang += -lgcc_s -lubsan --rtlib=compiler-rt

LDFLAGS.harden.gcc += -pie
LDFLAGS.harden.gcc += -Wl,-z,relro,-z,now
LDFLAGS.harden.gcc += -Wl,-z,noexecstack
LDFLAGS.harden.gcc += -lmcheck

LDFLAGS.harden.clang += -pie
LDFLAGS.harden.clang += -Wl,-z,relro,-z,now
LDFLAGS.harden.clang += -Wl,-z,noexecstack
LDFLAGS.harden.clang += -lmcheck

LDFLAGS += $(LDFLAGS.$(BUILD_TYPE).$(CCVARIANT)) $(LIBS)

# Conan dependencies
CONAN_MAKEFILE.true ?= conanbuildinfo.mak
CONAN_MAKEFILE.false ?= /dev/null
include $(CONAN_MAKEFILE.$(USE_CONAN))
CFLAGS += $(CONAN_CFLAGS)
CXXFLAGS += $(CONAN_CXXFLAGS)
CPPFLAGS += \
	`test -n "$(CONAN_INCLUDE_DIRS)" && printf -- '-I%s ' $(CONAN_INCLUDE_DIRS)`
CPPFLAGS += `test -n "$(CONAN_DEFINES)" && printf -- '-D%s ' $(CONAN_DEFINES)`
LDFLAGS += `test -n "$(CONAN_LIB_DIRS)" && printf -- '-L%s ' $(CONAN_LIB_DIRS)`
LDLIBS += `test -n "$(CONAN_LIBS)" && printf -- '-l%s ' $(CONAN_LIBS)`
EXELINKFLAGS += $(CONAN_EXELINKFLAGS)

CPPLINT_FILTERS += ,-build/c++11  # We like new language features
CPPLINT_FILTERS += ,-legal/copyright  # Copyright statements not neeeded for us
CPPLINT_FILTERS += ,-runtime/references  # We are okay with non-const refs
CPPLINT_FILTERS += ,-whitespace/newline  # OK: statements on the `else` line
CPPLINT_FILTERS += ,-whitespace/tab  # Tabs > spaces
CPPLINT_FILTERS += ,-readability/nolint  # Conflicts with clang-tidy
CPPLINT_FILTERS += ,-readability/casting  # Covered by clang-tidy
CPPLINT_FILTERS += ,-runtime/explicit  # Covered by clang-tidy
CPPLINT_FILTERS += ,-runtime/int  # Covered by clang-tidy
CPPLINT_FILTERS += ,-runtime/string  # Covered by clang-tidy
CPPLINT_FILTERS += ,-whitespace/braces \
	# False positives, covered by clang-format
CPPLINT_FILTERS += ,-whitespace/comments \
	# Flags OCLint pragmas; covered by clang-format

###### Build rules ######

#: Build all
all: PHONY $(TARGET)

# Since .PHONY is non-POSIX, we use this rule to mark phony targets
PHONY:

include deps

# Build target binary
$(TARGET): $(OBJS)
	$(CXX) $(CXXFLAGS) -o $(TARGET) $(OBJS) $(LDFLAGS)

#: Show help text
help: PHONY
	$(GREP) -A 1 '^#:' Makefile

#: Build archive containing code
#: Requires `tar` and `xz`
dist: PHONY $(ARCHIVE).tar.xz

$(ARCHIVE).tar.xz: clean
	$(MAKE) deps
	$(MKDIR) $(ARCHIVE)
	$(FIND) . -type d -not -path './$(ARCHIVE)/*' -not -path './$(ARCHIVE)' \
		-exec $(MKDIR) -p $(ARCHIVE)/'{}' ';'
	$(FIND) . -type f -not -path './$(ARCHIVE)/*' \
		-exec $(CP) -R '{}' $(ARCHIVE)/'{}' ';'
	XZ_OPTS='-9e' $(TAR) -cpJf $(ARCHIVE).tar.xz $(ARCHIVE)
	$(RM) $(ARCHIVE)

#: Update header dependencies
deps: $(SRCS) $(HEADERS)
	echo > deps
	$(FIND) . -name '*.c' -exec $(CC) -M -MM -MP '{}' ';' >> deps
	$(FIND) . -name '*.cpp' -exec $(CXX) -M -MM -MP '{}' ';' >> deps

#: Update compiler flags for conan.io dependencies
conanbuildinfo.mak: conanfile.txt
	-$(CONAN) install .

#: Run tests
test: PHONY

ARTIFACTS ?= \
	$(TARGET) \
	$(TARGET).wasm \
	$(ARCHIVE) \
	$(ARCHIVE).tar.xz \
	$(OBJS) \
	compile_commands.json \
	infer-out \
	$(OBJS:.o=.pvs) \
	$(OBJS:.o=.plist) \
	"*.ikos" \
	output.db \
	perf.data \
	perf.data.old \
	"callgrind.out*" \
	"*.profraw" \
	$(TARGET).profdata \
	"conanbuildinfo.*" \
	conaninfo.txt \
	graph_info.json \
	.mypy_cache \
	"*.ctu-info" \
	cov-int \
	cov-int.tar.xz \
	project.lnt \
	co-compiler.lnt \
	co-compiler.h \
	.cache

#: Clean build directory
clean: PHONY
	# Force expand wildcards
	$(RM) `printf '%s\n' $(ARTIFACTS)`

# Rules below are for dev purposes
# And thus may have worse compatibility across platforms

#: Build .gitignore file
gitignore: PHONY
	echo > .gitignore
	printf '%s\n' $(ARTIFACTS) >> .gitignore

#: Run clang-format <https://clang.llvm.org/docs/ClangFormat.html>
format: PHONY
	$(FIND) . '(' -name '*.h' -o -name '*.cpp' -o -name '*.c' ')' \
		-exec $(CLANG_FORMAT) -i --style=file --fallback-style=Google '{}' ';'

#: Set environment variables for debugging
env: PHONY
	@echo Please run eval '"`make env`"'! >&2
	# glibc heap consistency
	# https://www.gnu.org/s/libc/manual/html_node/Heap-Consistency-Checking.html
	@echo 'export MALLOC_CHECK_=3;'
	# Address sanitizer options
	# `detect_invalid_pointer_pairs` is broken...
	@echo 'export ASAN_OPTIONS=detect_invalid_pointer_pairs=0;'
	@echo 'export ASAN_OPTIONS=$$ASAN_OPTIONS,use_odr_indicator=true;'
	@echo 'export ASAN_OPTIONS=$$ASAN_OPTIONS,strict_string_checks=true;'
	@echo 'export ASAN_OPTIONS=$$ASAN_OPTIONS,check_initialization_order=true;'
	@echo 'export ASAN_OPTIONS=$$ASAN_OPTIONS,strict_init_order=true;'
	@echo 'export ASAN_OPTIONS=$$ASAN_OPTIONS,detect_stack_use_after_return=true;'
	# Hardened malloc <https://github.com/GrapheneOS/hardened_malloc>
	@echo 'export LD_PRELOAD="$(HARDENED_MALLOC):$$LD_PRELOAD";'

##### Linters #####
# For more tools, refer to
# <https://lefticus.gitbooks.io/cpp-best-practices/content/02-Use_the_Tools_Available.html>
# - MSVC analyzer/warnings
# - SonarLint
# - CLion Inspect Code

#: Run major linters
lint: PHONY \
	tidy \
	cppcheck \
	cpplint \
	gccanalyze \
	iwyu

#: Run clang-tidy <https://clang.llvm.org/extra/clang-tidy/>
tidy: PHONY compile_commands.json
	-$(CLANG_TIDY) `$(FIND) . -name '*.cpp'`

#: Run Cppcheck <https://cppcheck.sourceforge.io/>
cppcheck: PHONY compile_commands.json
	-$(CPPCHECK) -q --project=compile_commands.json \
		--addon=cppcheck \
		--addon=cppcheckdata \
		--addon=findcasts \
		--addon=misc \
		--addon=misra_9 \
		--addon=naming \
		--addon=threadsafety \
		--addon=y2038 \
		--enable=all \
		--suppress='missingIncludeSystem:*' \
		--inconclusive --inline-suppr
	-$(CPPCHECK) -q --project=compile_commands.json \
		--bug-hunting \
		--enable=style,performance,portability,information \
		--suppress='bughuntingUninit:*' \
		--suppress='missingIncludeSystem:*' \
		--inconclusive --inline-suppr
	# Too noisy
	# -$(CPPCHECK) -q --project=compile_commands.json \
	# 	--addon=misra.json \
	# 	--enable=all \
	# 	--suppress='missingIncludeSystem:*' \
	# 	--inconclusive --inline-suppr

#: Run Tscancode <https://github.com/Tencent/TscanCode>
tscancode: PHONY
	-$(TSCANCODE) --enable=all .

#: Run cpplint <https://github.com/cpplint/cpplint/>
cpplint: PHONY
	-$(CPPLINT) --verbose 0 --recursive --root . --filter='$(CPPLINT_FILTERS)' .

#: Run Flawfinder <https://github.com/david-a-wheeler/flawfinder>
flawfinder: PHONY
	-$(FLAWFINDER) .

#: Run OCLint <https://github.com/oclint/oclint/>
oclint: PHONY
	-$(OCLINT) \
		--enable-clang-static-analyzer \
		--enable-global-analysis \
		--extra-arg="$(CFLAGS.debug.clang) $(CPPFLAGS)" \
		--extra-arg=-std=c++17 \
		--extra-arg=-w \
		--disable-rule=AvoidPrivateStaticMembers \
		--disable-rule=HighCyclomaticComplexity \
		--disable-rule=HighNcssMethod \
		--disable-rule=HighNPathComplexity \
		--disable-rule=LongMethod \
		--disable-rule=ShortVariableName \
		`$(FIND) .  -name '*.cpp' -o -name '*.c'`

#: Run Infer <https://fbinfer.com/>
infer: PHONY compile_commands.json
	# Enable all warnings
	-$(INFER) run \
		--compilation-database compile_commands.json \
		--annotation-reachability \
		--bufferoverrun \
		--config-checks-between-markers \
		--config-impact-analysis \
		--cost \
		--eradicate \
		--immutable-cast \
		--litho-required-props \
		--loop-hoisting \
		--printf-args \
		--printf-args \
		--pulse \
		--quandary \
		--topl \
		--no-filtering

# PVS-Studio suppressed warnings:
# V108. Incorrect index type: 'foo[not a memsize-type]'. Use memsize type
# instead.
# V112. Dangerous magic number N used.
# V201. Explicit conversion from 32-bit integer type to memsize type.
# V202. Explicit conversion from memsize type to 32-bit integer type.
# V591. Non-void function must return value.
#     False positives on main().
# V831. Decreased performance. Consider replacing the call to the 'at()'
# method with the 'operator[]'.
# V2014. Don't use terminating functions in library code.
# V2506. MISRA. A function should have a single point of exit at the end.
# V2507. MISRA. The body of a loop\conditional statement should be enclosed
# in braces.
# V2542. MISRA. Function with a non-void return type should return a value
# from all exit paths.
#     Covered by compilers.
# V2563. MISRA. Array indexing should be the only form of pointer
# arithmetic and it should be applied only to objects defined as an array
# type.
#     Covered by clang-tidy.
# V2564. MISRA. There should be no implicit integral-floating conversion.
# V2570. MISRA. Operands of the logical '&&' or the '||' operators, the '!'
# operator should have 'bool' type.
# V2575. MISRA. The global namespace should only contain 'main', namespace
# declarations and 'extern "C"' declarations.
# V2578. MISRA. An identifier with array type passed as a function argument
# should not decay to a pointer.
#     False positives with assert().
# V2608. MISRA. The 'static' storage class specifier should be used in all
# declarations of object and functions that have internal linkage.
# V801. Decreased performance. It is better to redefine the N function argument
# as a reference. Consider replacing 'const T' with 'const .. &T' / 'const ..
# *T'.
#     Covered by other linters.
# V2003. Explicit conversion from 'float/double' type to signed integer type.
# V2008. Cyclomatic complexity: NN. Consider refactoring the 'Foo' function.
#     Covered by other linters.
PVS_SUPPRESS += V108,
PVS_SUPPRESS += V112,
PVS_SUPPRESS += V201,
PVS_SUPPRESS += V202,
PVS_SUPPRESS += V591,
PVS_SUPPRESS += V831,
PVS_SUPPRESS += V2014,
PVS_SUPPRESS += V2506,
PVS_SUPPRESS += V2507,
PVS_SUPPRESS += V2542,
PVS_SUPPRESS += V2563,
PVS_SUPPRESS += V2564,
PVS_SUPPRESS += V2570,
PVS_SUPPRESS += V2575,
PVS_SUPPRESS += V2608,
PVS_SUPPRESS += V801,
PVS_SUPPRESS += V2003,
PVS_SUPPRESS += V2008,

#: Run PVS-Studio <https://pvs-studio.com/en/>
pvs: PHONY $(OBJS:.o=.pvs)
	-$(PLOG_CONVERTER) \
		-a 'GA:1,2,3;64:1,2,3;OP:1,2,3;CS:1,2,3;MISRA:1,2,3' \
		-d `printf "%s" $(PVS_SUPPRESS)` \
		-t tasklist \
		$(OBJS:.o=.pvs)

#: Run Flint++ <https://github.com/JossWhittle/FlintPlusPlus>
flint: PHONY
	$(FLINT) .

#: Run GCC static analyzer
gccanalyze: PHONY
	-$(GCC) $(CFLAGS.debug.gcc) -std=gnu18 $(CPPFLAGS) \
		-fanalyzer `$(FIND) . -name '*.c'` $(LDFLAGS) -o /dev/null
	-$(GXX) $(CFLAGS.debug.gcc) -std=c++17 $(CPPFLAGS) \
		-fanalyzer `$(FIND) . -name '*.cpp'` $(LDFLAGS) -o /dev/null

#: Run include-what-you-use <https://include-what-you-use.org/>
iwyu: PHONY compile_commands.json
	$(IWYU) -p .

#: Run IKOS <https://github.com/NASA-SW-VnV/ikos/>
#: Writes results to *.ikos
ikos: PHONY
	# Dead code analysis is too noisy
	$(FIND) . '(' -name '*.cpp' -o -name '*.c' ')' -exec $(IKOS) \
		-a='*,-dca,-sound,-uio,-upa' \
		-f=text \
		--report-file='{}.ikos' \
		-march=native \
		'{}' ';'

#: Build package for uploading to Coverity Scan <https://scan.coverity.com/>
coverity: PHONY clean
	-$(COV_BUILD) --dir cov-int $(MAKE) all
	$(TAR) cJf cov-int.tar.xz cov-int

# header file '__string__' repeatedly included but has no header guard
PCLINT_SUPPRESS += -e451
# repeated include file '__file__'
PCLINT_SUPPRESS += -e537
# implicit conversion (__context__) from __type__ to __type__
PCLINT_SUPPRESS += -e713
# loss of sign (__context__) (__type__ to __type__)
PCLINT_SUPPRESS += -e732
# execution completed producing __integer__ primary and __integer__
# supplemental messages (__integer__ total) after processing __integer__
# module(s)
PCLINT_SUPPRESS += -e900
# return statement before end of function __symbol__
PCLINT_SUPPRESS += -e904
# implicit boolean conversion from __type__
PCLINT_SUPPRESS += -e909
# implicit promotion from __type__ to __type__
PCLINT_SUPPRESS += -e911
# __left/right__ operand of binary operator '__operator__', originally of type
# __type__, will be converted to type __type__ due to usual arithmetic
# conversions with __left/right__ operand of type __type__
PCLINT_SUPPRESS += -e912
# implicit arithmetic conversion (__context__) from __type__ to __type__
PCLINT_SUPPRESS += -e915
# implicit conversion from __type__ to __type__ due to function prototype
PCLINT_SUPPRESS += -e917
# implicit conversion (__context__) from lower precision type __type__ to
# higher precision type __type__
PCLINT_SUPPRESS += -e919
# explicit cast from __type__ to __type__
PCLINT_SUPPRESS += -e920 -e921 -e922 -e923
# data member __symbol__ declared as type __type__
PCLINT_SUPPRESS += -e935
# padding of __integer__ bytes needed to align __string__ on a __integer__ byte
# boundary
PCLINT_SUPPRESS += -e958
# nominal structure size of '__integer__' bytes is not a whole multiple of its
# alignment of '__integer__' bytes
PCLINT_SUPPRESS += -e959
# use of modifier or type '__name__' outside of a typedef
PCLINT_SUPPRESS += -e970
# use of plain char
PCLINT_SUPPRESS += -e971
# creating a temporary of type __type__
PCLINT_SUPPRESS += -e1901
# implicit non-trivial destructor generated for __symbol__
PCLINT_SUPPRESS += -e1907
# implicit call of converting constructor __symbol__
PCLINT_SUPPRESS += -e1911
# implicit call of conversion function from class __symbol__ to type __type__
PCLINT_SUPPRESS += -e1912
# use of functional-style cast to convert from type __type__ to type __type__
PCLINT_SUPPRESS += -e1946
# use of default capture (__string__) in lambda expression
PCLINT_SUPPRESS += -e1970
# reference to __data member/member function__ __symbol__ of __symbol__ does
# not use an explicit 'this->'
PCLINT_SUPPRESS += -e3901
# side effects on right hand of logical operator, '__string__'
PCLINT_SUPPRESS += -e9007
# body should be a compound statement
PCLINT_SUPPRESS += -e9012
# increment/decrement operation combined with other operation with side-effects
PCLINT_SUPPRESS += -e9049
# dependence placed on operator precedence
PCLINT_SUPPRESS += -e9050
# function '__name__' is recursive
PCLINT_SUPPRESS += -e9070
# result of assignment operator used in __string__
PCLINT_SUPPRESS += -e9084
# dependence placed on C++ operator precedence
PCLINT_SUPPRESS += -e9113
# implicit conversion of underlying type of integer cvalue expression from
# __underlying-type__ to __underlying type__
PCLINT_SUPPRESS += -e9114
# implicit conversion of underlying type of floating point cvalue expression
# from __underlying-type__ to __underlying type__
PCLINT_SUPPRESS += -e9116
# implicit conversion from integer to floating point type
PCLINT_SUPPRESS += -e9115
# implicit conversion from __underlying-type__ to __underlying-type__ changes
# signedness of underlying type
PCLINT_SUPPRESS += -e9117
# implicit conversion of integer to smaller underlying type (__type__ to
# __type__)
PCLINT_SUPPRESS += -e9119
# cast of integer cvalue expression changes signedness
PCLINT_SUPPRESS += -e9125
# plain character data mixed with non-plain-character data
PCLINT_SUPPRESS += -e9128
# bitwise operator '__operator__' applied to signed underlying type
PCLINT_SUPPRESS += -e9130
# __left/right__ side of logical operator '__operator__' is not a postfix
# expression
PCLINT_SUPPRESS += -e9131
# boolean expression required for operator '__string__'
PCLINT_SUPPRESS += -e9133
# multiple declarators in a declaration
PCLINT_SUPPRESS += -e9146
# non-private data member __symbol__ within a non-POD structure
PCLINT_SUPPRESS += -e9150
# __left/right__ side of logical operator '__operator__' is not a primary
# expression
PCLINT_SUPPRESS += -e9240
# C++ style comment used
PCLINT_SUPPRESS += -e9260
# array subscript applied to variable __symbol__ declared with non-array type
# __type__
PCLINT_SUPPRESS += -e9264
# non-POD class __symbol__ defined with 'struct' keyword
PCLINT_SUPPRESS += -e9437
# preprocessing directive in call to function __symbol__
PCLINT_SUPPRESS += -e9501
# continue statement encountered
PCLINT_SUPPRESS += -e9254
# hexadecimal escape sequence used
PCLINT_SUPPRESS += -e9204
# plain character data used with prohibited operator __string__
PCLINT_SUPPRESS += -e9209
# infinite loop via while
PCLINT_SUPPRESS += -e716
# boolean argument(s) to __equality-operator__
PCLINT_SUPPRESS += -e731
# union initialization
PCLINT_SUPPRESS += -e708
# old-style c comment
PCLINT_SUPPRESS += -e1904
# use of c-style cast (__type__ to __type__)
PCLINT_SUPPRESS += -e1924
# union __symbol__ declared
PCLINT_SUPPRESS += -e9018
# constant value used in Boolean context (__string__)
PCLINT_SUPPRESS += -e506
# boolean argument to __arithmetic/bitwise__ operator '__operator__'
PCLINT_SUPPRESS += -e514
# local variable __symbol__ declared in __symbol__ not subsequently referenced
PCLINT_SUPPRESS += -e529
# named parameter __symbol__ of '__virtual/non-virtual__' function __symbol__
# not subsequently referenced
PCLINT_SUPPRESS += -e715
# boolean used as argument __integer__ to function __symbol__
PCLINT_SUPPRESS += -e730
# omitted braces within initializer
PCLINT_SUPPRESS += -e940
# too few initializers for aggregate __symbol__ of type __type__
PCLINT_SUPPRESS += -e943
# pointer subtraction
PCLINT_SUPPRESS += -e947
# __string__ variable __symbol__ of type __type__ is neither const nor atomic
PCLINT_SUPPRESS += -e956
# use of c-style cast to void (__type__ to __type__)
PCLINT_SUPPRESS += -e1954
# empty declaration
PCLINT_SUPPRESS += -e1972
# departure from MISRA switch syntax: __detail__
PCLINT_SUPPRESS += -e9042
# function parameter __symbol__ modified
PCLINT_SUPPRESS += -e9044
# switch case lacks unconditional break or throw
PCLINT_SUPPRESS += -e9090
# missing unconditional break from final switch case
PCLINT_SUPPRESS += -e9077
# implicit conversion (__context__) from type __type__ to type __type__
PCLINT_SUPPRESS += -e907
# implicit pointer assignment conversion (__context__) from __type__ to
# __type__
PCLINT_SUPPRESS += -e916
# function __symbol__ could be marked with a 'pure' semantic
PCLINT_SUPPRESS += -e979
# static member __symbol__ could be accessed using a nested name specifier
# instead of applying operator '__string__' to an instance of class __symbol__
PCLINT_SUPPRESS += -e1705
# non-reference parameter __symbol__ of function __symbol__ could be reference
# to const
PCLINT_SUPPRESS += -e1746
# variable __symbol__ has '__static/thread__' storage duration and non-POD type
# __type__
PCLINT_SUPPRESS += -e1756
# implicit conversion from Boolean (__context__) (__type__ to __type__)
PCLINT_SUPPRESS += -e1785
# function __symbol__ parameter __integer__ has same unqualified type (__type__)
# as previous parameter
PCLINT_SUPPRESS += -e9403
# predicate of conditional operator has non-Boolean type __type__
PCLINT_SUPPRESS += -e9178
# condition of '__string__' statement has non-Boolean type __type__
PCLINT_SUPPRESS += -e9177
# global declaration of symbol __symbol__
PCLINT_SUPPRESS += -e9141
# bitwise operator '__operator__' used with non-constant operands of differing
# underlying types (__underlying-type__ and __underlying-type__)
PCLINT_SUPPRESS += -e9172
# the shift value is at least the precision of the MISRA C++ underlying type of
# the left hand side
PCLINT_SUPPRESS += -e9136
# the result of the __operator__ operator applied to an object with an
# underlying type of __underlying-type__ must be cast to __type__ in this
# context
PCLINT_SUPPRESS += -e9126
# cast of integer cvalue expression to larger type
PCLINT_SUPPRESS += -e9123
# plain character expression used with non-permitted operator '__string__'
PCLINT_SUPPRESS += -e9112
# __incrementing/decrementing__ pointer
PCLINT_SUPPRESS += -e9017
# performing pointer arithmetic via __addition/subtraction__
PCLINT_SUPPRESS += -e9016
# non-member function __symbol__ returns reference type __type__
PCLINT_SUPPRESS += -e1929
# static variable __symbol__ of type __type__ has a non-trivial destructor
PCLINT_SUPPRESS += -e1937
# declaration of __symbol__ of type __type__ requires an exit-time destructor
PCLINT_SUPPRESS += -e1945
# function __symbol__, previously designated pure, __reason__
PCLINT_SUPPRESS += -e453
# relational operator __string__ applied to pointers
PCLINT_SUPPRESS += -e946
# multiple definitions of function __symbol__
PCLINT_SUPPRESS += -e2467
# qualifier '__string__' precedes typedef type __type__
PCLINT_SUPPRESS += -e9183
# potentially confusing __hexadecimal/octal__ escape sequence usage
PCLINT_SUPPRESS += -e9039
# converting integer constant expression, which evaluates to __integer__ but is
# not an integer literal equal to zero or one, to bool
PCLINT_SUPPRESS += -e1564
# variable __symbol__ of type __type__ not initialized by definition
PCLINT_SUPPRESS += -e901
# for statement index variable __symbol__ modified in body
PCLINT_SUPPRESS += -e850
# both sides have side effects
PCLINT_SUPPRESS += -e931
# ignoring return value of function __symbol__
PCLINT_SUPPRESS += -e534
# cast of cvalue expression from integer to floating point type
PCLINT_SUPPRESS += -e9121
# cast of cvalue expression from floating point to integer type
PCLINT_SUPPRESS += -e9122

#: Run PC-Lint Plus <https://www.gimpel.com/pclp.html>
pclint: compile_commands.json
	$(PCLINT_CONFIG) \
		--compiler=$(CCVARIANT) \
		--compiler-bin=$(CXX) \
		--config-output-lnt-file=co-compiler.lnt \
		--config-output-header-file=co-compiler.h \
		--generate-compiler-config
	$(PCLINT_CONFIG) \
		--compiler=$(CCVARIANT) \
		--compilation-db=compile_commands.json \
		--config-output-lnt-file=project.lnt \
		--generate-project-config
	$(PCLINT) co-compiler.lnt -w4 -summary $(PCLINT_SUPPRESS) -max_threads=1 \
		project.lnt

#### Dynamic analysis ####

#: Run Valgrind Memcheck <https://valgrind.org/>
valgrind: PHONY $(TARGET)
	$(VALGRIND) \
		--leak-check=full \
		--leak-resolution=med \
		--track-origins=yes \
		--partial-loads-ok=no \
		--expensive-definedness-checks=yes \
		--freelist-vol=100000000 \
		--main-stacksize=167772160 \
		-s \
		./$(TARGET)

#: Run Callgrind/Cachegrind for profiling <https://valgrind.org/>
callgrind: PHONY $(TARGET)
	$(VALGRIND) \
		--tool=callgrind \
		--cache-sim=yes \
		--branch-sim=yes \
		--collect-jumps=yes \
		--dump-instr=yes \
		--main-stacksize=167772160 \
		./$(TARGET)

#### Code coverage ####

#: Merge *.profraw files into a profdata file
profdata: PHONY
	$(LLVM_PROFDATA) merge -sparse *.profraw -o $(TARGET).profdata

#: Display coverage recorded by profdata file
cov: PHONY $(TARGET)
	$(LLVM_COV) show ./$(TARGET) --instr-profile=$(TARGET).profdata

###### Inference rules ######

.SUFFIXES: .o .c .cpp .pvs .cc

.c.o:
	$(CC) $(CFLAGS) -c $<

.cpp.o:
	$(CXX) $(CXXFLAGS) -c $<

# Generate analysis files for PVS-Studio
.c.pvs:
	# Add comments for non-commercial usage
	# <https://github.com/viva64/how-to-use-pvs-studio-free>
	$(PVS_STUDIO_HOWTO) -c 3 $<
	$(CC) $(CFLAGS) $< -E -o $@.pvsi
	$(PVS_STUDIO) \
		--cfg .pvs-studio \
		--source-file $< \
		--i-file $@.pvsi \
		--output-file $@
	rm $@.pvsi

.cpp.pvs:
	# Add comments for non-commercial usage
	# <https://github.com/viva64/how-to-use-pvs-studio-free>
	$(PVS_STUDIO_HOWTO) -c 3 $<
	$(CXX) $(CXXFLAGS) $< -E -o $@.pvsi
	$(PVS_STUDIO) \
		--cfg .pvs-studio \
		--source-file $< \
		--i-file $@.pvsi \
		--output-file $@
	rm $@.pvsi

.c.cc:
	$(CAT) $(HEADERS) $< | grep -v '^#include "' > $@

.cpp.cc:
	$(CAT) $(HEADERS) $< | grep -v '^#include "' > $@

# TODOs:
# Integrate CI & fuzzing
# cf. <https://github.com/cpp-best-practices/cpp_starter_project>
