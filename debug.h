#ifndef DEBUG_H_
#define DEBUG_H_

#include <cassert>
#include <cfenv>
#include <iostream>

template <class IStream>
[[maybe_unused]] inline void FailOnBadInput(IStream &is = std::cin) {
	is.exceptions(std::ios_base::failbit);
}

[[maybe_unused]] inline void TrapFp([[maybe_unused]] bool force = false) {
	// Trap on floating point particularities (performance hit should be
	// negligible)
	// Only works on glibc
#ifdef FE_NOMASK_ENV
	int res = feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);
#else
	int res = force;
#endif
	assert(res == 0 && "Unable to enable floating point exceptions.");
}

#endif  // DEBUG_H_
